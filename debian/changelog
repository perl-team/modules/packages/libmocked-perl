libmocked-perl (0.09-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.
  * Apply multi-arch hints. + libmocked-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 04 Dec 2022 01:23:53 +0000

libmocked-perl (0.09-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 13:13:25 +0100

libmocked-perl (0.09-5) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Do not ship useless README
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Add pod-error.patch and no-dot-in-inc.patch

 -- Florian Schlichting <fsfs@debian.org>  Tue, 03 Jul 2018 21:04:11 +0200

libmocked-perl (0.09-4) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Christoph Berg ]
  * Remove myself from Uploaders.

  [ Ryan Niebur ]
  * Update ryan52's email address

 -- Christoph Berg <myon@debian.org>  Wed, 09 Dec 2009 14:06:16 +0100

libmocked-perl (0.09-3) unstable; urgency=low

  [ Ryan Niebur ]
  * Add myself to Uploaders
  * add missing build dep on liburi-perl (Closes: #531234)

  [ gregor herrmann ]
  * Minimize debian/rules, bump debhelper build dependency and debian/compat.
  * Add /me to Uploaders.
  * debian/copyright: switch to new format.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 May 2009 15:58:04 +0200

libmocked-perl (0.09-2) unstable; urgency=low

  * Set Maintainer to Debian Perl Group.

 -- Christoph Berg <myon@debian.org>  Fri, 22 May 2009 14:59:22 +0200

libmocked-perl (0.09-1) unstable; urgency=low

  * Initial Release.

 -- Christoph Berg <myon@debian.org>  Fri, 22 May 2009 14:16:57 +0200
